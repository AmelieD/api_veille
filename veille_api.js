
var veilles = []; //Je crée un tableau dans une variable pour recevoir le contenu du JSON

//Le jsoncallback dans lequel je génére les boutons des veilles avec leurs titres troncatés
function jsoncallback(data){
  veilles = data.veilles;

  $(function() {
    $("#pagination").pagination({
      items: veilles.length,
      itemsOnPage: 28,
      cssStyle: 'light-theme'
    });
  });

  count_up = veilles.length
  count_down = veilles.length -28

  veilles = veilles.reverse();

  veilles.forEach(function(veille) {
     count_up--;
     if (count_up >= count_down) {
       $("#button").append("<button type='button' onclick='load_veille(" + veille.id + ")'>" + trunc(veille.title) + "</button>");
     }
   });
}

//Récupération du JSON
$(document).ready(function() {
   $.getJSON("http://veille.popschool.fr/api/?api=veille&action=getAll",jsoncallback);
});

//Charge le contenu de la veille en fonction de l'id du bouton
function load_veille(id_veille) {
    empty();

    //Id match
    veilles.forEach(function(veille) {
      if(veille.id == id_veille) {

        //Lorsque la veille est chargé, fait apparaitre la div auteur et le bouton "update"
        document.getElementById("author_show").style.display = "block";
        document.getElementById("update").style.display = "inline";

        //Pour chaque élément de la veille, rempli la div et l'input du formulaire concernés
        $("#id").text("Id : "+veille.id);
        document.getElementById("id_input").value = veille.id;

        $("#title").text("Titre : "+veille.title);
        document.getElementById("title_input").value = veille.title;

        $("#date").text("Date : "+veille.date);
        document.getElementById("date_input").value = veille.date;

        $("#type").text("Type : "+veille.type);
        document.getElementById("type_input").value = veille.type;

        $("#description").text("Description : "+veille.description);
        document.getElementById("description_input").value = veille.description;

        $("#link").text("Lien : "+veille.link);
        document.getElementById("link_input").value = veille.link;

        $("#source").text("Source : "+veille.source);
        document.getElementById("source_input").value = veille.source;

        $("#id_user").text(veille.id_user);
        document.getElementById("id_user_input").value = veille.id_user;

        $("#firstname").text(veille.firstname);
        document.getElementById("firstname_input").value = veille.firstname;

        $("#filename").text(veille.filename);
        document.getElementById("filename_input").value = veille.filename;
        console.log(veille);
      }
    });
}

//Modifie l'affichage des boutons et affiche le formulaire en appuyant sur le bouton pour modifier la veille
function update_form() {
  document.getElementById("form").style.display="block";
  document.getElementById("abort").style.display="inline";
  document.getElementById("update").style.display="none";
}

//Modifie l'affichage des boutons et affiche le formulaire en appuyant sur le bouton pour créer la veille
function load_form() {
  document.getElementById("form").style.display="block";
  document.getElementById("abort").style.display="inline";
  empty();
}

//Reviens à l'état initial
function abort() {
  document.getElementById("form").style.display="none";
  document.getElementById("abort").style.display="none";
  empty();
}

//Fonction appelé pour vider le contenu du formulaire et des divs contenant les détails de la veille sélectionné
function empty() {
  document.getElementById("title_input").value = "";
  document.getElementById("id_input").value = "";
  document.getElementById("date_input").value = "";
  document.getElementById("type_input").value = "";
  document.getElementById("description_input").value = "";
  document.getElementById("link_input").value = "";
  document.getElementById("source_input").value = "";
  document.getElementById("firstname_input").value = "";
  document.getElementById("filename_input").value = "";

  $("#title").empty();
  $("#id").empty();
  $("#date").empty();
  $("#type").empty();
  $("#description").empty();
  $("#link").empty();
  $("#source").empty();
  $("#filename").empty();
  document.getElementById("author_show").style.display = "none";
}

//Fonction pour troncater le titre
function trunc(elem) {
  if(elem.length > 13) {
    return elem.substr(0,13).concat('...');
  } else {
    return elem
  }
}

//Enregistrement de la veille contenu dans le formulaire
function save() {
  var f = new Object();
  f.title = document.getElementById("title_input").value
  f.id = document.getElementById("id_input").value
  f.date = document.getElementById("date_input").value
  f.type = document.getElementById("type_input").value
  f.description = document.getElementById("description_input").value
  f.link = document.getElementById("link_input").value
  f.source = document.getElementById("source_input").value
  f.firstname = document.getElementById("firstname_input").value
  f.id_user = document.getElementById("id_user_input").value
  f.filename = document.getElementById("filename_input").value
    console.log(f)

    //Demande les infos de l'utilisateur
    email_user = prompt("e-mail : ","");
    if(f.firstname == "") {
      f.firstname = prompt("Prénom ?","Philippe");
    }
    if(f.id_user == '') {
      f.id_user = prompt("Votre id ?","1");
    }
    password_user = prompt("password : ","");

    //Envois des données
    $.ajax({
      url: "http://veille.popschool.fr/api/",
      data: {api: "veille", action:"save", email: email_user, password: password_user, veille:JSON.stringify(f)},
      complete: function(r) {
        console.log(r.responseText);
        // setTimeout(function() { window.location.href='index.html'; }, 5000);
      }
    });
  }
